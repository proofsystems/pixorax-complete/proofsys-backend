<?php

namespace Api\Countries;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Application\ApiController;
use Domain\Country\CountryRepository;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class CountriesController extends ApiController
{
  public function __construct(CountryRepository $country){
    parent::__construct($country,'Country');
    $this->country = $country;
  }
}
