<?php
  $controller = '\Api\Countries\CountriesController';

  Route::group(['prefix' => 'countries'],function() use ($controller){
    Route::get('all',$controller.'@all');
  });

  Route::resource('countries', $controller)->middleware('auth:api');
