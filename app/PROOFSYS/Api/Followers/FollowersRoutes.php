<?php
Route::middleware(['auth:api'])->group(function () {

  $controller = '\Api\Followers\FollowersController';

  Route::group(['prefix' => 'followers'],function() use ($controller){
    Route::group(['middleware'=>'api'],function() use ($controller){
      Route::post('follow/{user_id}',$controller.'@follow');
      Route::get('followers',$controller.'@followers');
      Route::get('all',$controller.'@all');
    });
  });

  Route::resource('followers', $controller)->middleware('auth:api');
});
