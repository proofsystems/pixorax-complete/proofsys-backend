<?php

namespace Api\Users;

use App\Http\Controllers\Controller;
use Application\ApiController;
use Domain\Follower\FollowerRepository;
use Domain\Notification\NotificationRepository;
use Domain\Posts\PostRepository;
use Domain\User\UserRepository;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UsersController extends ApiController
{
  public function __construct(UserRepository $user,NotificationRepository $notification, FollowerRepository $follower, PostRepository $post){
    parent::__construct($user,'User');
    $this->user = $user;
    $this->notification = $notification;
    $this->follower = $follower;
    $this->post = $post;
  }
  /**
   * Get the guard to be used during authentication.
   *
   * @return \Illuminate\Contracts\Auth\Guard
   */
  public function guard()
  {
      return Auth::guard('api');
  }

  /**
   * Description: Update User
   * Date: September 27, 2019 (Friday)
   **/
  public function updateUser(Request $request) {
    $validator = Validator::make($request->all(), [
      'email' => 'string|email|max:255|unique:users,email',
      'username' => 'string|max:255|unique:users,username',
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(),400);
    }
    $repository = $this->user->updateUser($request->all());
    return response()->json([
      'message' => $this->module.' Successfully Updated',
      'data'=> $this->user->find(auth('api')->user()->id),
    ],200);
  }


  public function updatePassword(Request $request) {
    $validator = Validator::make($request->all(), [
      'old_password' => 'required',
      'password' => 'required|min:6',
      'confirm_password' => 'required_with:password|same:password|min:6'
    ]);

    $validator->after(function ($validator) use ($request) {
        if (!Hash::check($request->old_password,$this->guard()->user()->password)) {
            $validator->errors()->add('old_password', "Old password doesn't match");
        }
    });

    if ($validator->fails()) {
        return response()->json(['message'=>$validator->errors()->first()],406);
    }

    $user = $this->guard()->user();
    $user->password = Hash::make($request->password);
    $user->save();
    return response()->json(['message'=>'Password successfully updated.']);

  }

  /**
   * Description: User's notification
   * Date: September 25, 2019 (Wednesday)
   **/
  public function notifications() {
    return $this->notification->own();
  }

  /**
   * Description: User's notification
   * Date: September 25, 2019 (Wednesday)
   **/
  public function all_notifications() {
    return $this->notification->all();
  }

  /**
   * Description: Followers
   * Date: October 30, 2019 (Wednesday)
   **/
  public function followers() {
    return $this->follower->followers();
  }

  /**
   * Description: Following
   * Date: October 30, 2019 (Wednesday)
   **/
  public function following() {
    return $this->follower->following();
  }

  /**
   * Description: Suggestions
   * Date: November 12, 2019 (Tuesday)
   **/
  public function suggestions() {
    return $this->user->suggestions();
  }

  /**
   * Description: User Search
   * Date: November 12, 2019 (Tuesday)
   **/
  public function search(Request $request) {
    return $this->user->search($request);
  }

  /**
   * Description: Users Following
   * Date: November 13, 2019 (Wednesday)
   **/
  public function user_following($id) {
    return $this->follower->user_following($id);
  }

  /**
   * Description: Users Following
   * Date: November 13, 2019 (Wednesday)
   **/
  public function user_followers($id) {
    return $this->follower->user_followers($id);
  }

  /**
   * Description: Followers Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_followers(Request $request) {
    return $this->follower->paginated_followers($request);
  }

  /**
   * Description: Following Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_following(Request $request) {
    return $this->follower->paginated_following($request);
  }

  /**
   * Description: Return paginated Notifications
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_notifications(Request $request) {
    return $this->notification->paginated_notifications($request);
  }

  /**
   * Description: Users Following
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_user_following($id) {
    return $this->follower->paginated_user_following($id);
  }

  /**
   * Description: Users Following
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_user_followers($id) {
    return $this->follower->paginated_user_followers($id);
  }

  /**
   * Description: Other User's Feed
   * Date: December 05, 2019 (Thursday)
   **/
  public function other_user_feed($id) {
    return $this->post->other_user_feed($id);
  }
}
