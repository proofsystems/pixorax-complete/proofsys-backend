<?php
$controller = '\Api\Users\UsersController';
Route::group(['prefix' => 'users','middleware'=>'api'],function() use ($controller){
    Route::group(['prefix' => 'paginated'],function() use ($controller){
      Route::get('followers',$controller.'@paginated_followers');
      Route::get('following',$controller.'@paginated_following');
      Route::get('notifications',$controller.'@paginated_notifications');
      Route::get('{user_id}/following',$controller.'@paginated_user_following');
      Route::get('{user_id}/followers',$controller.'@paginated_user_followers');
    });
    Route::get('{user_id}/following',$controller.'@user_following');
    Route::get('{user_id}/followers',$controller.'@user_followers');
    Route::get('{user_id}/feed',$controller.'@other_user_feed');
    Route::get('notifications',$controller.'@notifications');
    Route::get('all_notifications',$controller.'@all_notifications');
    Route::put('change-password',$controller.'@updatePassword');
    Route::get('all',$controller.'@all');
    Route::post('edit',$controller.'@updateUser');

    Route::get('followers',$controller.'@followers');
    Route::get('following',$controller.'@following');

    Route::get('suggestions',$controller.'@suggestions');
    Route::post('search',$controller.'@search');
});

Route::resource('users', $controller);
