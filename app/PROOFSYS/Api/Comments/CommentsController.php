<?php

namespace Api\Comments;

use App\Http\Controllers\Controller;
use Application\ApiController;
use Domain\Comments\CommentRepository;
use Domain\Like\LikeRepository;
use Domain\Notification\NotificationRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommentsController extends ApiController
{
  public function __construct(CommentRepository $comment, LikeRepository $like,NotificationRepository $notify){
    parent::__construct($comment,'Comment');
    $this->comment = $comment;
    $this->like = $like;
    $this->notify = $notify;
  }

  public function store(Request $request){
    $comment =  $request->all();
    $comment['user_id']=Auth('api')->user()->id;
    $comment = $this->comment->create($comment);
    $this->notify->notify('post','comment',$request->post_id,$comment->id);
    return response()->json([
      'message' => 'Comment Successfully Created',
      'data' => $this->comment->find($comment->id)
    ],200);
  }

    /**
   * Description: Like Comment
   * Date: September 21, 2019 (Saturday)
   **/
    public function likeComment($comment_id) {
      return $this->like->like($comment_id,'comment');
    }
}
