<?php

namespace Api\Auth;

use Illuminate\Support\Facades\Auth;
use Application\ApiController;
use Illuminate\Http\Request;

use Domain\User\UserRepository;

use Illuminate\Support\Facades\Validator;


class AuthController extends ApiController
{

  protected $user;
  /**
   * Create a new AuthController instance.
   *
   * @return void
   */
  public function __construct(UserRepository $user)
  {
      $this->middleware('auth:api')->except('register','login','socialLogin');
      $this->user = $user;
  }

  /**
   * Get a JWT token via given credentials.
   *
   * @param  \Illuminate\Http\Request  $request
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function login(Request $request)
  {
    $user = $this->user->findForPassport($request->email);
    if ($user) {
      $request['email']=$user->email;
    } else {
      return response()->json(['message'=>'Your credentials are incorrect. Please try again'],401);
    }
    $token = $this->user->token($request);
    if ($token==401) {
      return response()->json(['message'=>'Your credentials are incorrect. Please try again'],401);
    }
    if ($token==400) {
      return response()->json(['message'=>'Invalid Request. Please enter a email or a password.'],400);
    }

    $profile = $this->user->profile($token['access_token']);
    return response()->json(compact('token','profile'));
  }

  /**
   * Description: Social Login
   * Date: September 03, 2019 (Tuesday)
   * Payloads
   * - provider
   * - access_token
   **/
  public function socialLogin(Request $request) {
    $token = $this->user->social($request);
    $profile = $this->user->profile($token['access_token']);
    return response()->json(compact('token','profile'));
  }

  /**
   * Get the authenticated User
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function me()
  {
      return response()->json($this->guard()->user());
  }

  public function register(Request $request)
  {

    $validator = Validator::make($request->all(), [
      'fname' => 'required|string|max:255',
      'lname' => 'required|string|max:255',
      'email' => 'required|string|email|max:255|unique:users,email',
      'username' => 'required|string|max:255|unique:users,username',
      'password' => 'required|string|min:6'
    ]);

    if ($validator->fails()) {
      return response()->json($validator->errors(),400);
    }

    try {
      $user =  $this->user->register($request);
      if ($user) {
        $token = $this->user->token($request);
        $profile = $this->user->profile($token['access_token']);
        return response()->json(compact('token','profile'));
      }
    } catch (Exception $e) {
      return response()->json($e,500);
    }
  }

  public function logout()
  {
      auth()->user()->tokens->each(function ($token, $key) {
          $token->delete();
      });
      return response()->json('Logged out successfully', 200);
  }

  /**
   * Refresh a token.
   *
   * @return \Illuminate\Http\JsonResponse
   */
  public function refresh()
  {
      return $this->respondWithToken($this->guard()->refresh());
  }

  /**
   * Get the token array structure.
   *
   * @param  string $token
   *
   * @return \Illuminate\Http\JsonResponse
   */
  protected function respondWithToken($token)
  {
      return response()->json([
          'access_token' => $token,
          'token_type' => 'bearer',
          'expires_in' => $this->guard()->factory()->getTTL() * 60
      ]);
  }

  /**
   * Get the guard to be used during authentication.
   *
   * @return \Illuminate\Contracts\Auth\Guard
   */
  public function guard()
  {
      return Auth::guard('api');
  }
}
