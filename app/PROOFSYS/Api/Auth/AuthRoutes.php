<?php


Route::group([

    'prefix' => 'auth'

], function ($router) {
    Route::post('/password/email', '\Api\Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', '\Api\Auth\ResetPasswordController@reset');

    Route::group(['middleware'=>'api'],function ($router) {
      Route::post('logout', '\Api\Auth\AuthController@logout');
      Route::post('refresh', '\Api\Auth\AuthController@refresh');
      Route::get('me', '\Api\Auth\AuthController@me');
    });

    Route::post('/login', '\Api\Auth\AuthController@login');
    Route::post('/socialLogin', '\Api\Auth\AuthController@socialLogin');
    Route::post('/register', '\Api\Auth\AuthController@register');
});
