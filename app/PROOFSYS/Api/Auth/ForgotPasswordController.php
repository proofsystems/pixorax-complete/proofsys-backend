<?php

namespace Api\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;

class ForgotPasswordController extends Controller
{
  use SendsPasswordResetEmails;

  protected function sendResetLinkResponse(Request $request, $response)
  {
    return response(['message'=>trans($response)]);
  }

  protected function sendResetLinkFailedResponse(Request $request, $response)
  {
    if (User::where('email',$request->email)->count() == 0) {
      return response()->json(['status'=>"Email doesn't exist."],406);
    }
    return response(['status'=>trans($response)],422 );
  }

}
