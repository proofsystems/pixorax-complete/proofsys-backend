<?php

namespace Api\HashtagFollowings;

use App\Http\Controllers\Controller;
use Application\ApiController;
use Domain\HashtagFollowing\HashtagFollowingRepository;
use Domain\Hashtag\HashtagRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HashtagFollowingsController extends ApiController
{
  public function __construct(HashtagFollowingRepository $hashtagfollowing, HashtagRepository $hashtag){
    parent::__construct($hashtagfollowing,'HashtagFollowing');
    $this->hashtagfollowing = $hashtagfollowing;
    $this->hashtag = $hashtag;
  }

  public function store(Request $request){
    $hash = $this->hashtag->find($request->hashtag_id)->makeHidden('posts');
    $check = $this->hashtagfollowing->check($request->hashtag_id);

    if ($check > 0) {
      $this->hashtagfollowing->unfollow($request->hashtag_id);
      return response()->json([
        'message' => $hash->hashtag.' Successfully Unfollowed',
        'data' => $hash
      ],200);
    }

    $this->hashtagfollowing->create($request->all());
    return response()->json([
      'message' => $hash->hashtag.' Successfully Followed',
      'data' => $hash
    ],200);
  }
}
