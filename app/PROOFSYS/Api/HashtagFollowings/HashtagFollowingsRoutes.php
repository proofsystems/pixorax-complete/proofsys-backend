<?php
Route::middleware(['auth:api'])->group(function () {

  $controller = '\Api\HashtagFollowings\HashtagFollowingsController';

  Route::group(['prefix' => 'hashtags'],function() use ($controller){
    Route::group(['middleware'=>'api'],function() use ($controller){
      Route::get('all',$controller.'@all');
    });
  });

  Route::resource('hashtags', $controller)->middleware('auth:api');
});
