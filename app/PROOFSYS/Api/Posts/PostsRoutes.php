<?php
Route::middleware(['auth:api'])->group(function () {

  $controller = '\Api\Posts\PostsController';

  Route::group(['prefix' => 'posts'],function() use ($controller){
    Route::get('getFive',$controller.'@getFive');
    Route::get('own',$controller.'@own');
    Route::get('like/{post_id}',$controller.'@likePost');

    Route::group(['prefix' => 'hashtags'],function() use ($controller){
      Route::group(['prefix' => 'view/{hashtag}'],function() use ($controller){
        Route::get('paginated',$controller.'@paginated_view_hashtag');
        Route::get('/',$controller.'@view_hashtag');
      });
      Route::get('search',$controller.'@search_hashtag');
    });

    Route::group(['prefix' => 'favorites'],function() use ($controller){
      Route::get('today',$controller.'@today_favorites');
      Route::get('week',$controller.'@week_favorites');
      Route::get('month',$controller.'@month_favorites');
      Route::get('/',$controller.'@favorites');
    });

    Route::group(['prefix' => 'users'],function() use ($controller){
      Route::get('{user_id}',$controller.'@user_posts');
    });

    Route::group(['prefix' => 'trends'],function() use ($controller){
      Route::get('today',$controller.'@today_trends');
      Route::get('week',$controller.'@week_trends');
      Route::get('month',$controller.'@month_trends');
      Route::get('/',$controller.'@trends');
    });

    Route::get('all',$controller.'@all');
    Route::group(['prefix' => 'paginated'],function() use ($controller){
      Route::get('own',$controller.'@paginated_own');
      Route::get('all',$controller.'@paginated');
      Route::get('others/{user_id}',$controller.'@paginated_other_pics');
      Route::get('others',$controller.'@paginated_other');
      Route::get('feeds',$controller.'@feeds_paginated');
    });

    Route::get('feeds',$controller.'@feeds');
    Route::get('suggestions',$controller.'@suggestions');
  });

  Route::resource('posts', $controller)->middleware('auth:api');
});
