<?php

namespace Api\Posts;

use App\Http\Controllers\Controller;
use Application\ApiController;
use Carbon\Carbon;
use Domain\Content\ContentRepository;
use Domain\Like\LikeRepository;
use Domain\Notification\NotificationRepository;
use Domain\Posts\PostRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class PostsController extends ApiController
{
  public function __construct(PostRepository $post, NotificationRepository $notification, LikeRepository $like, ContentRepository $content){
    parent::__construct($post,'Post');
    $this->post = $post;
    $this->notification = $notification;
    $this->like = $like;
    $this->content = $content;
  }
  /**
   * Get storage path for images, photos or pictures.
   *
   * @return string
   */
  public static function storagePath()
  {
    return 'images';
  }

  public static function createImageDirectory()
  {
    Storage::makeDirectory('public/images/' . self::storagePath(), 0775, true);
  }

  public static function createImageFieldDirectory($module,$field)
  {
    Storage::makeDirectory('public/images/'.$module.'/'.$field, 0775, true);
  }

  public function store(Request $request){
    $video_format = [
        '','mp4', 'm4a', 'm4v', 'f4v', 'f4a', 'm4b', 'm4r', 'f4b', 'mov', '3gp', '3gp2', '3g2', '3gpp', '3gpp2', 'ogg', 'oga', 'ogv', 'ogx', 'wmv', 'wma', 'flv', 'avi', 'MP4', 'M4A', 'M4V', 'F4V', 'F4A', 'M4B', 'M4R', 'F4B', 'MOV', '3GP', '3GP2', '3G2', '3GPP', '3GPP2', 'OGG', 'OGA', 'OGV', 'OGX', 'WMV', 'WMA', 'FLV', 'AVI', 'gif', 'GIF'
    ];

    $image_format = [
        '','tiff', 'jpg', 'jpeg','png', 'TIFF', 'JPG', 'JPEG', 'PNG'
    ];

    $upload =  $request->all();

    DB::beginTransaction();

    try {
      if (!is_null($request->caption)) {
        $this->post->save_hashtags($request->caption);
      }
      $post = $this->post->create($upload);
      if($files=$request->file('content')){
          $i=0;
        foreach ($files as $file) {
          self::createImageDirectory();
          self::createImageFieldDirectory('contents','content');

          $time = Carbon::now()->format('YmdTHis');
          $name = $time.rand(111, 999) . '.' . $file->getClientOriginalExtension();
          $path = 'images/contents/content';
          $storage_path = 'app/public/'.$path;
            if (array_search($file->getClientOriginalExtension(),$image_format)>0) {
              $type = 'image';
            };

            if (array_search($file->getClientOriginalExtension(),$video_format)>0) {
              $type = 'video';
            }

          $content = ['content'=>'storage/'.$path.'/'.$name,'type'=>$type];
          $post->contents()->create($content);
          $file->move(storage_path($storage_path), $name);
        }
      }
      DB::commit();
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'message' => $e->getMessage(),
      ],$e->getCode());
    }

    $post['user']=Auth('api')->user();
    $this->notification->sendNotification($post,'POST','New image has been uploaded.');

    return response()->json([
      'message' => 'Post Successfully Created',
      'data' => $post->toArray()
    ],200);
  }

  public function destroy($id){
    $post = $this->post->find($id);
    if ($post->user_id != auth('api')->user()->id) {
      return response()->json([],406);
    }
    foreach ($post->contents as $content) {
      $content->forceDelete();
    }
    $this->post->delete($id);
    return response()->json([
      'message' => $this->module.' Successfully Deleted'
    ],200);
  }

  /**
   * Description: Return 5 post
   * Date: October 05, 2019 (Saturday)
   **/
  public function getFive() {
    return $this->post->getFive();
  }

  /**
   * Description: User's Own Post
   * Date: September 21, 2019 (Saturday)
   **/
  public function own() {
    return $this->post->own();
  }

  /**
   * Description: Like Post
   * Date: September 21, 2019 (Saturday)
   **/
  public function likePost($post_id) {
    return $this->like->like($post_id,'post');
  }

  /**
   * Description: All Trends
   * Date: September 24, 2019 (Tuesday)
   **/
  public function trends() {
    return $this->post->trends();
  }

  /**
   * Description: Trend POst
   * Date: September 24, 2019 (Tuesday)
   **/
  public function today_trends() {
    return $this->post->today_trends();
  }

  /**
   * Description: Trend POst for this week
   * Date: September 24, 2019 (Tuesday)
   **/
  public function week_trends() {
    return $this->post->week_trends();
  }

  /**
   * Description: Trend Post for this month
   * Date: September 24, 2019 (Tuesday)
   **/
  public function month_trends() {
    return $this->post->month_trends();
  }

  /**
   * Description: User's Favorites
   * Date: September 24, 2019 (Tuesday)
   **/
  public function favorites() {
    return $this->post->favorites();
  }

  /**
   * Description: User's favorites for today
   * Date: September 25, 2019 (Wednesday)
   **/
  public function today_favorites() {
    return $this->post->today_favorites();
  }

  /**
   * Description: User's favorites for this week
   * Date: September 25, 2019 (Wednesday)
   **/
  public function week_favorites() {
    return $this->post->week_favorites();
  }

  /**
   * Description: User's favorites for this month
   * Date: September 25, 2019 (Wednesday)
   **/
  public function month_favorites() {
    return $this->post->month_favorites();
  }

  /**
   * Description: Users Feed
   * Date: October 30, 2019 (Wednesday)
   **/
  public function feeds() {
    return $this->post->feeds();
  }

  /**
   * Description: User's Feed Paginated
   * Date: December 18, 2019 (Wednesday)
   **/
  public function feeds_paginated() {
    return $this->post->feeds_paginated();
  }

  /**
   * Description: Suggestions
   * Date: November 12, 2019 (Tuesday)
   **/
  public function suggestions() {
    return $this->post->suggestions();
  }

  /**
   * Description: Return Post Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated(Request $request) {
    return $this->post->paginated($request->all());
  }

  /**
   * Description: Return Post Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_own(Request $request) {
    return $this->post->paginated_own($request->all());
  }

  /**
   * Description: Return Post Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_other(Request $request) {
    return $this->post->paginated_other($request->all());
  }

  /**
   * Description: Return Post Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_other_pics($id) {
    return $this->post->paginated_other_pics($id);
  }

  /**
   * Description: User's Posts
   * Date: November 18, 2019 (Monday)
   **/
  public function user_posts($id) {
    return $this->post->user_posts($id);
  }

  /**
   * Description: Search Hashtag
   * Date: December 04, 2019 (Wednesday)
   **/
  public function search_hashtag(Request $request) {
    return $this->post->search_hashtag($request->key);
  }

  /**
   * Description: View Hashtag
   * Date: December 05, 2019 (Thursday)
   **/
  public function view_hashtag($hashtag) {
    return $this->post->view_hashtag($hashtag);
  }

  /**
   * Description: View Hashtag Paginated
   * Date: December 10, 2019 (Tuesday)
   **/
  public function paginated_view_hashtag($hashtag) {
    return $this->post->paginated_view_hashtag($hashtag);
  }
}
