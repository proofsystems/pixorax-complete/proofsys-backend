<?php

namespace Api\Stories;

use App\Http\Controllers\Controller;
use Application\ApiController;
use Domain\Story\StoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class StoriesController extends ApiController
{
  public function __construct(StoryRepository $story){
    parent::__construct($story,'Story');
    $this->story = $story;
  }

  /**
   * Description: User's Own Stories
   * Date: October 07, 2019 (Monday)
   **/
  public function own() {
    return $this->story->own();
  }

  /**
   * Description: Return Stories Per User
   * Date: October 07, 2019 (Monday)
   **/
  public function perUser() {
    return $this->story->perUser();
  }

  /**
   * Description: Show Story
   * Date: November 13, 2019 (Wednesday)
   **/
  public function view_story($id) {
    return $this->story->view_story($id);
  }

  /**
   * Description: Get users who viewed the story.
   * Date: November 13, 2019 (Wednesday)
   **/
  public function get_view_story($id) {
    return $this->story->get_view_story($id);
  }
}
