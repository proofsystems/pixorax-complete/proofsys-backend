<?php
  $controller = '\Api\Stories\StoriesController';

  Route::group(['prefix' => 'stories'],function() use ($controller){
    Route::get('view/{story_id}',$controller.'@get_view_story');
    Route::post('view/{story_id}',$controller.'@view_story');
    Route::get('all',$controller.'@all');
    Route::get('own',$controller.'@own');
    Route::get('per-user',$controller.'@perUser');
  });

  Route::resource('stories', $controller)->middleware('auth:api');
