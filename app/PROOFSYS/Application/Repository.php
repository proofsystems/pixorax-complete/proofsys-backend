<?php

namespace Application;

// base model for eloquent repository

class Repository{

  protected $entity;

  public function __construct($entity)
  {
      $this->entity = $entity;
  }


  public function filter($input){
    $search = isset($input['search'])?$input['search']:null;
    $orderBy = isset($input['orderBy'])?$input['orderBy']:[];
    $data = $this->entity;
    $first = $this->entity->first();

    if(!$first){
      return $data;
    }
      $columns = array_keys($first->getAttributes());

      foreach($columns as $key => $value){
        if(in_array($value,['created_at','updated_at','deleted_at'])){
          unset($columns[$key]);
        }
      }


      foreach($orderBy as $key => $column){
        $data = $data->orderBy($key,$column);
      }


      if($search){
        $data = $data->where(function ($query) use ($search,$columns){
                    foreach($columns as $column){
                      $query->orWhere($column, 'like', '%'.$search.'%');
                    }
                });
      }
    return $data;
  }

  public function paginateWithFilters($input){
    $search = isset($input['search'])?$input['search']:null;
    $page = isset($input['page'])?$input['page']:1;
    $size = isset($input['size'])?$input['size']:10;
    $orderBy = isset($input['orderBy'])?$input['orderBy']:[];
    $data = $this->filter($input)->paginate($size);
    return $data;
  }

  public function create(array $data)
  {
    return $this->entity->create($data);
  }

  public function find($id)
  {
    return $this->entity->find($id);
  }

  public function findBy($columns,$value)
  {
    if(is_array($columns)){
      $query = $this->entity;
      foreach($columns as $key => $column){
        $query = ($key == 0)?$query->where($column,$value):$query->orWhere($column,$value);
      }
      return $query->get();
    }

    return $this->entity->where($columns,$value)->get();
  }

  public function findFirstBy($column,$value){
    return $this->entity->where($column,$value)->first();
  }


  public function update(array $data, $identifier)
  {
    $entity = $this->entity->find($identifier);
    //unset($data['jira_key']);

    foreach($data as $key => $input){
      $entity->{$key} = $input;
    }
    return $entity->save();
  }

  public function all()
  {
    return $this->entity->all();
  }


  public function delete($id)
  {
    $this->entity->find($id)->delete();
  }

  public function relation($id,$relation){
    return $this->entity->find($id)->{$relation};
  }

  public function findBySlug($slug){
    return $this->entity->where('slug',$slug)->first();
  }

  public function getByCategory($category_name){
    return null;
  }

  /**
   * Description: User Profile
   * Date: December 05, 2019 (Thursday)
   **/
  public function user() {
    return Auth('api')->user();
  }
}
