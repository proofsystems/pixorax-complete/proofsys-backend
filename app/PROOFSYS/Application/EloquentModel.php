<?php

namespace Application;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Askedio\SoftCascade\Traits\SoftCascadeTrait;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
class EloquentModel extends Model
{
		use SoftDeletes;
    use SoftCascadeTrait;

		public function scopeWithHas($query, $relation, $constraint){
    	return $query->whereHas($relation, $constraint)
                 ->with([$relation => $constraint]);
		}

    protected $appends = ['created_at_timezone','updated_at_timezone','image_address'];

		public function getImageAddressAttribute(){

      if($this->image && Storage::exists('/public/' .$this->image)){
        return asset('/storage/' . $this->image);
      }

      return asset('/placeholders/content.png');

    }

    public function getCreatedAtTimezoneAttribute() {
      return Carbon::parse($this->created_at)->format('Y-m-d H:i:s e');
    }
    public function getUpdatedAtTimezoneAttribute() {
      return Carbon::parse($this->updated_at)->format('Y-m-d H:i:s e');
    }
}
