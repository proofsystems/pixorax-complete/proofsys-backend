<?php

namespace Domain\ViewedStory;

use App\User;
use Application\Repository;
use Carbon\Carbon;
use Domain\ViewedStory\ViewedStory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ViewedStoryRepository extends Repository
{

  protected $story;

  public function __construct(ViewedStory $story){
    parent::__construct($story);
    $this->story = $story;
  }
}
