<?php

namespace Domain\ViewedStory;

use App\User;
use Application\EloquentModel;
use Application\Traits\Imaging;
use Domain\Notification\Notification;
use Domain\Story\Story;
use Domain\ViewedStory\ViewedStory;

class ViewedStory extends EloquentModel
{

    protected $table = 'user_viewed_story';

    protected $fillable =
    [
        'user_id',
        'story_id',
    ];

    protected $with = ['user','story'];

    public function user() {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function story() {
        return $this->belongsTo(Story::class,'user_id','id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->user_id = Auth('api')->user()->id;
        });

        static::updating(function ($model) {
            static::updateImage($model);
        });

        static::deleting(function ($model) {
            static::deleteImage($model);
        });
    }
}



