<?php

namespace Domain\Content;

use Application\Repository;
use Carbon\Carbon;
use Domain\Content\Content;
use Illuminate\Http\Request;

class ContentRepository extends Repository
{
  protected $content;

  public function __construct(Content $content){
    parent::__construct($content);
    $this->content = $content;
  }
}
