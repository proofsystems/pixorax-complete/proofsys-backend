<?php

namespace Domain\Content;

use Application\EloquentModel;
use Application\Traits\Imaging;
use Domain\Notification\Notification;

class Content extends EloquentModel
{
  use Imaging;

  protected $table = 'contents';

  protected $fillable =
  [
    'post_id',
    'content',
    'type',
  ];

  protected $image_fields = ['content'];

  public function contentable()
  {
    return $this->morphTo();
  }

  public static function boot()
  {
    parent::boot();

    static::creating(function ($model) {
    });

    static::updating(function ($model) {
      static::updateImage($model);
    });

    static::deleting(function ($model) {
      static::deleteImage($model);
    });
  }
}



