<?php

namespace Domain\HashtagFollowing;

use Application\Repository;
use Domain\HashtagFollowing\HashtagFollowing;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HashtagFollowingRepository extends Repository
{

  protected $hashtag;

  public function __construct(HashtagFollowing $hashtag){
    parent::__construct($hashtag);
    $this->hashtag = $hashtag;
  }

  public function check($hashtag_id)
  {
    return $this->hashtag
      ->where('hashtag_id',$hashtag_id)
      ->where('user_id',$this->user()->id)
      ->count();
  }

  /**
   * Description: Unfollow
   * Date: December 05, 2019 (Thursday)
   **/
  public function unfollow($hashtag_id) {
    return $this->hashtag
      ->where('hashtag_id',$hashtag_id)
      ->where('user_id',$this->user()->id)
      ->forceDelete();
  }
}
