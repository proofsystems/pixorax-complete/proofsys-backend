<?php

namespace Domain\HashtagFollowing;

use App\User;
use Application\EloquentModel;
use Domain\Hashtag\Hashtag;

class HashtagFollowing extends EloquentModel
{

    protected $table = 'hashtag_following';

    protected $fillable =
    [
        'user_id',
        'hashtag_id',
    ];

    protected $with = ['user'];

    public function user() {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function hashtag() {
        return $this->belongsTo(Hashtag::class,'user_id','id');
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->user_id = Auth('api')->user()->id;
        });

        static::updating(function ($model) {
        });

        static::deleting(function ($model) {
        });
    }
}



