<?php

namespace Domain\Hashtag;

use Application\Repository;
use Domain\Hashtag\Hashtag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HashtagRepository extends Repository
{

  protected $hashtag;

  public function __construct(Hashtag $hashtag){
    parent::__construct($hashtag);
    $this->hashtag = $hashtag;
  }
}
