<?php

namespace Domain\Hashtag;

use Application\EloquentModel;
use Domain\HashtagFollowing\HashtagFollowing;
use Domain\Posts\Post;

class Hashtag extends EloquentModel
{

    protected $table = 'hashtags';

    protected $fillable =
    [
        'hashtag',
    ];

    protected $appends =['posts_count','followers_count','is_following','posts','posts_paginated'];

    /**
     * Description: Get Posts
     * Date: December 06, 2019 (Friday)
     **/
    public function getPostsAttribute() {
      return Post::where('caption','like','%'.$this->hashtag.'%')
      ->orderBy('created_at','DESC')
      ->get();
    }

    /**
     * Description: Get Posts
     * Date: December 06, 2019 (Friday)
     **/
    public function getPostsPaginatedAttribute() {
      return Post::where('caption','like','%'.$this->hashtag.'%')
      ->orderBy('created_at','DESC')
      ->paginate(10);
    }

    /**
     * Description: Posts Count
     * Date: December 06, 2019 (Friday)
     **/
    public function getPostsCountAttribute() {
      return Post::where('caption','like','%'.$this->hashtag.'%')->count();
    }

    /**
     * Description: Get Follower Count
     * Date: December 06, 2019 (Friday)
     **/
    public function getFollowersCountAttribute() {
      return HashtagFollowing::where('hashtag_id',$this->id)->count();
    }

    /**
     * Description: Is Following
     * Date: December 06, 2019 (Friday)
     **/
    public function getIsFollowingAttribute() {
      return HashtagFollowing::where('hashtag_id',$this->id)
      ->where('user_id',Auth('api')->user()->id)
      ->count();
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
        });

        static::updating(function ($model) {
        });

        static::deleting(function ($model) {
        });
    }
}



