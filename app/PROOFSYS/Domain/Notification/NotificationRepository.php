<?php

namespace Domain\Notification;

use Domain\Comments\Comment;
use Domain\Like\Like;
use Domain\Notification\Notification;
use Domain\Posts\Post;
use GuzzleHttp\Client;


class NotificationRepository
{
  protected $like;
  protected $notification, $post;

  public function __construct(Notification $notification, Post $post)
  {
      $this->notification = $notification;
      $this->post = $post;
  }

  /**
   * Description: User's Notification top 10
   * Date: September 25, 2019 (Wednesday)
   **/
  public function own() {
    return $this->notification->where('user_id',auth('api')->user()->id)->take(10)
      ->orderBy('created_at','DESC')
      ->get();
  }

  /**
   * Description: Rebuild Object
   * Date: December 08, 2019 (Sunday)
   **/
  public function rebuild($data) {
    $i=0;
    foreach ($data as $not) {
      $obj[$i] = $not;
      if ($not->notifiable_type == 'Domain\\Comments\\Comment') {
        $obj[$i]['content'] = $this->post->find($not->notifiable->post_id)->makeHidden('comments','likes');
      } else if ($not->notifiable_type == 'Domain\\Posts\\Post') {
        $obj[$i]['content'] = $this->post->find($not->notifiable->id)->makeHidden('comments','likes');
      }
      $i++;
    }
    return $obj;
  }

  /**
   * Description: User's Notification all
   * Date: September 25, 2019 (Wednesday)
   **/
  public function all() {
    $notifications = $this->notification->where('user_id',auth('api')->user()->id)
      ->orderBy('created_at','DESC')
      ->get();
    return $this->rebuild($notifications);
  }

  /**
   * Description: Paginated Notifications
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_notifications() {
    return $this->notification->where('user_id',auth('api')->user()->id)
      ->orderBy('created_at','DESC')
      ->paginate(10);
  }

  /**
   * Description: Send Notification
   * Date: September 18, 2019 (Wednesday)
   **/
  public function sendNotification($detail,$type,$msg) {
    $client = new Client();
    $client->request('POST', 'https://onesignal.com/api/v1/notifications', [
      'json' => [
        'app_id' => env('ONESIGNAL_APP_ID','3215f35a-4476-4d52-a270-f165066b36f1'),
        'included_segments' => ['Subscribed Users'],
        'headings' => [
          'en' =>  $msg
        ],
        'contents' => [
          'en' => ($detail->caption)? $detail->caption : 'Uploaded'
        ],
        "ios_badgeType" => "Increase",
        "ios_badgeCount" => 1,
        "large_icon" => env('APP_URL') . '/images/notif-logo.png',
        "data" => [
          "page" => $type,
          "id" => $detail->id,
          "user" => $detail->user->fname.' '.$detail->user->lname,
        ]
      ],
      'headers' => [
        'https'         => 'https://onesignal.com/api/v1/notifications',
        'Authorization' => 'Basic YjJhMGU5ZDktY2QyYy00ZWE2LWFjMzQtMDFmNzljOWE1YWJk',
        'Content-Type'  => 'application/json'

      ]
    ]);
    return $client;
  }

  /**
   * Description: Notification per action
   * Date: September 25, 2019 (Wednesday)
   **/
  public function notify($post_type, $action,$destination_id,$source_id) {
    $actions = [
      'comment' => 'commented on your',
      'like' => 'liked your',
    ];

    $message =  $actions[$action].' '.$post_type;
    if($post_type=='post') {
      $post_type='Domain\Posts\Post';
      $item = Post::find($destination_id);
    } else if($post_type=='comment'){
      $post_type='Domain\Comments\Comment';
      $item = Comment::find($destination_id);
    }

    if ($item->user_id == auth('api')->user()->id) {
      return;
    }

    if ($action == 'comment') {
      $source_type = 'Domain\Comments\Comment';
      $source = Comment::find($source_id);
    } else if ($action == 'like') {
      $source_type = 'Domain\Like\Like';
      $source = Like::find($source_id);
    }

    $data = $item->notifications()->create([
      'user_id'=>$item->user_id,
      'sourceable_id'=>$source->id,
      'sourceable_type'=>$source_type,
      'sender_user_id'=>auth()->user()->id,
      'message'=>$message,
      'action'=>$action,
    ]);
    return $data;
  }

  /**
   * Description: Notify Following
   * Date: October 29, 2019 (Tuesday)
   **/
  public function notify_follow($user_id,$following_id) {
    if ($user_id == auth('api')->user()->id) {
      return;
    }

    $message =  'followed you.';
    $type='Domain\Follower\Follower';
    $action='follow';

    $check = $this->notification->where('notifiable_id',$following_id)
      ->where('notifiable_type',$type)
      ->where('user_id',$user_id)
      ->where('sender_user_id',auth()->user()->id)
      ->where('action',$action)
      ->count();

    $data = [
      'user_id'=>$user_id,
      'notifiable_id'=>$following_id,
      'notifiable_type'=>$type,
      'sender_user_id'=>auth()->user()->id,
      'message'=>$message,
      'action'=>$action,
    ];
    if ($check==0) {
      $data = $this->notification->create($data);
    }
    return $data;
  }
}
