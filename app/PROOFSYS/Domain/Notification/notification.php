<?php

namespace Domain\Notification;

use App\User;
use Application\EloquentModel;

class Notification extends EloquentModel
{
  protected $table = 'notifications';

  protected $fillable =
  [
    'user_id',
    'notifiable_id',
    'notifiable_type',
    'sourceable_id',
    'sourceable_type',
    'sender_user_id',
    'message',
    'action'
  ];

  protected $with =['user','sender','notifiable','sourceable'];

  public function notifiable()
  {
    return $this->morphTo();
  }

  public function sourceable()
  {
    return $this->morphTo();
  }

  public function user()
  {
    return $this->belongsTo(User::class,'user_id');
  }

  public function sender()
  {
    return $this->belongsTo(User::class,'sender_user_id');
  }
}
