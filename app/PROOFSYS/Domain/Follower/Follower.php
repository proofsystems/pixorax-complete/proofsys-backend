<?php

namespace Domain\Follower;

use App\User;
use Application\EloquentModel;
use Domain\Notification\Notification;

class Follower extends EloquentModel
{
  protected $table = 'followers';

  protected $fillable =
  [
    'user_id',
    'following_user_id',
  ];

  protected $with =['user','following'];

  public function likeable()
  {
    return $this->morphTo();
  }

  public function user()
  {
    return $this->belongsTo(User::class,'user_id');
  }

  public function notifications()
  {
    return $this->morphMany(Notification::class, 'notifiable');
  }

  public function following() {
    return $this->belongsTo(User::class,'following_user_id');
  }

  public static function boot()
  {
    parent::boot();

    static::creating(function ($model) {
      $model->user_id = auth('api')->user()->id;
    });

    static::deleting(function ($model) {
      $model->notifications()->forceDelete();
    });
  }
}
