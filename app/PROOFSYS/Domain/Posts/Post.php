<?php

namespace Domain\Posts;

use App\User;
use Application\EloquentModel;
use Application\Traits\Imaging;
use Domain\Comments\Comment;
use Domain\Content\Content;
use Domain\Like\Like;
use Domain\Notification\Notification;

class Post extends EloquentModel
{
    protected $table = 'posts';

    protected $fillable =
    [
        'user_id',
        'caption',
    ];

    protected $appends = ['is_liked'];

    protected $softCascade = ['comments'];

    protected $cascadeDeleteMorph = ['likes', 'notifications', 'contents'];

    protected $with = ['comments','likes','user','contents'];
    protected $withCount = ['likes'];

    public function comments()
    {
        return $this->hasMany(Comment::class, 'post_id');
    }

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function contents()
    {
        return $this->morphMany(Content::class, 'contentable');
    }

    public function user() {
      return $this->belongsTo(User::class,'user_id');
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notifiable');
    }

    /**
     * Description: return if liked
     * Date: November 17, 2019 (Sunday)
     **/
    public function getIsLikedAttribute() {
        return $this->likes()->where(['likeable_id'=>$this->id,'user_id'=>$this->user_id])->count();
    }

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->user_id = auth('api')->user()->id;
        });

        static::updating(function ($model) {
        });

        static::deleting(function ($model) {
            $model->notifications()->delete();
            $model->likes()->forceDelete();
            $model->comments()->forceDelete();
        });
    }
}



