<?php

namespace Domain\Posts;

use Application\Repository;
use Carbon\Carbon;
use Domain\Follower\Follower;
use Domain\HashtagFollowing\HashtagFollowing;
use Domain\Hashtag\Hashtag;
use Domain\Like\Like;
use Domain\Posts\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostRepository extends Repository
{

  protected $post, $like, $hashtag;

  public function __construct(Post $post, Like $like, Hashtag $hashtag){
    parent::__construct($post);
    $this->post = $post;
    $this->like = $like;
    $this->hashtag = $hashtag;
  }

  /**
   * Description: All Modified
   * Date: September 27, 2019 (Friday)
   **/
  public function all()
  {
    $items = $this->post->orderBy('created_at','DESC')->get();
    foreach ($items as $key => $item) {
      $check = $item->likes->where('user_id',$this->user()->id)->count();
      if ($check>0) {
        $item['isLiked']=1;
      } else {
        $item['isLiked']=0;
      }
    }
    return $items;
  }

  /**
   * Description: Show 5 item per paginate
   * Date: September 27, 2019 (Friday)
   **/
  public function getFive()
  {
    $itemsPaginated = $this->post->orderBy('created_at','DESC')->paginate(5);

    $itemsTransformed = $itemsPaginated
        ->getCollection()
        ->map(function($item) {
            return [
                'id' => $item->id,
                'user_id' => $item->user_id,
                'caption' => $item->caption,
                'contents' => $item->contents,
                'created_at' => $item->created_at,
                'updated_at' => $item->updated_at,
                'deleted_at' => $item->deleted_at,
                'likes_count' => $item->likes_count,
                'comments' => $item->comments,
                'likes' => $item->likes,
                'isLiked'=>$item->likes->where('user_id',$this->user()->id)->count()
            ];
    })->toArray();

    return $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
        $itemsTransformed,
        $itemsPaginated->total(),
        $itemsPaginated->perPage(),
        $itemsPaginated->currentPage(), [
            'path' => \Request::url(),
            'query' => [
                'page' => $itemsPaginated->currentPage()
            ]
        ]
    );
  }

  /**
   * Description: User's Post
   * Date: September 21, 2019 (Saturday)
   **/
  public function own() {
    return $this->post->where('user_id',auth()->user()->id)->orderBy('created_at','DESC')->get();
  }

  /**
   * Description: Trends
   * Date: September 24, 2019 (Tuesday)
   **/
  public function today_trends() {
    return $this->post
     ->where(DB::raw('date(created_at)'),date('Y-m-d'))
     ->having('likes_count','>',0)
     ->orderBy('likes_count','DESC')
     ->take(10)
     ->get();
  }

  /**
   * Description: All Trends
   * Date: September 24, 2019 (Tuesday)
   **/
  public function trends() {
    return $this->post
     ->having('likes_count','>',0)
     ->orderBy('likes_count','DESC')
     ->get();
  }

  /**
   * Description: Trend POst for this week
   * Date: September 24, 2019 (Tuesday)
   **/
  public function week_trends() {
    $now = Carbon::now();
    $weekStartDate = $now->startOfWeek()->format('Y-m-d');
    $weekEndDate = $now->endOfWeek()->format('Y-m-d');
    
    return $this->post
     ->whereBetween(DB::raw('date(created_at)'),[$weekStartDate,$weekEndDate])
     ->having('likes_count','>',0)
     ->orderBy('likes_count','DESC')
     ->take(10)
     ->get();
  }

  /**
   * Description: Trend Post for this month
   * Date: September 24, 2019 (Tuesday)
   **/
  public function month_trends() {
    $now = Carbon::now();
    $monthStartDate = $now->startOfMonth()->format('Y-m-d');
    $monthEndDate = $now->endOfMonth()->format('Y-m-d');
    
    return $this->post
     ->whereBetween(DB::raw('date(created_at)'),[$monthStartDate,$monthEndDate])
     ->having('likes_count','>',0)
     ->orderBy('likes_count','DESC')
     ->take(10)
     ->get();
  }

  /**
   * Description: User's Favorites
   * Date: September 24, 2019 (Tuesday)
   **/
  public function favorites() {
    return $this->post->whereHas('likes',function($q){
      $q->where('user_id',auth()->user()->id);
    })->get();
  }

  /**
   * Description: User's favorites for today
   * Date: September 25, 2019 (Wednesday)
   **/
  public function today_favorites() {
    return $this->post->whereHas('likes',function($q){
      $q->where('user_id',auth()->user()->id)
        ->where(DB::raw('date(created_at)'),date('Y-m-d'));
    })->get();
  }

  /**
   * Description: User's favorites for this week
   * Date: September 25, 2019 (Wednesday)
   **/
  public function week_favorites() {
    $now = Carbon::now();
    $weekStartDate = $now->startOfWeek()->format('Y-m-d');
    $weekEndDate = $now->endOfWeek()->format('Y-m-d');

    return $this->post->whereHas('likes',function($q) use ($weekStartDate,$weekEndDate){
      $q->where('user_id',auth()->user()->id)
        ->whereBetween(DB::raw('date(created_at)'),[$weekStartDate,$weekEndDate]);
    })->get();
  }

  /**
   * Description: User's favorites for this month
   * Date: September 25, 2019 (Wednesday)
   **/
  public function month_favorites() {
    $now = Carbon::now();
    $monthStartDate = $now->startOfMonth()->format('Y-m-d');
    $monthEndDate = $now->endOfMonth()->format('Y-m-d');

    return $this->post->whereHas('likes',function($q) use ($monthStartDate,$monthEndDate){
      $q->where('user_id',auth()->user()->id)
        ->whereBetween(DB::raw('date(created_at)'),[$monthStartDate,$monthEndDate]);
    })->get();
  }

  /**
   * Description: User's Feed
   * Date: October 30, 2019 (Wednesday)
   **/
  public function feeds() {
    $hts = HashtagFollowing::where('user_id',$this->user()->id)->get();
    $ffs = Follower::where('user_id',$this->user()->id)->get();

    $post = $this->post->orderBy('created_at','DESC')
    ->orWhere('user_id',$this->user()->id);

    foreach ($hts as $ht) {
      $post = $post->orWhere('caption','like','%#'.$ht->hashtag.'%');
    }

    foreach ($ffs as $ff) {
      $post = $post->orWhere('user_id',$ff->following->id);
    }
    $post = $post->get();
    return $post;
  }

  /**
   * Description: User's Feed Paginated
   * Date: December 18, 2019 (Wednesday)
   **/
  public function feeds_paginated() {
    $hts = HashtagFollowing::where('user_id',$this->user()->id)->get();
    $ffs = Follower::where('user_id',$this->user()->id)->get();

    $post = $this->post->orderBy('created_at','DESC')
    ->orWhere('user_id',$this->user()->id);

    foreach ($hts as $ht) {
      $post = $post->orWhere('caption','like','%#'.$ht->hashtag.'%');
    }

    foreach ($ffs as $ff) {
      $post = $post->orWhere('user_id',$ff->following->id);
    }
    $post = $post->paginate(10);
    return $post;
  }

  /**
   * Description: Suggestions
   * Date: November 12, 2019 (Tuesday)
   **/
  public function suggestions() {
    return $this->post->where('user_id','!=',$this->user()->id)->inRandomOrder()->get();
  }

  /**
   * Description: Post Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated($request) {
    $itemsPaginated = $this->post->orderBy('created_at','DESC')->paginate(10);

    $itemsTransformed = $itemsPaginated
        ->getCollection()
        ->map(function($item) {
            return [
                'id' => $item->id,
                'contents' => $item->contents,
            ];
    })->toArray();

    return $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
        $itemsTransformed,
        $itemsPaginated->total(),
        $itemsPaginated->perPage(),
        $itemsPaginated->currentPage(), [
            'path' => \Request::url(),
            'query' => [
                'page' => $itemsPaginated->currentPage()
            ]
        ]
    );
  }

  /**
   * Description: Own Post Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_own($request) {
    $itemsPaginated = $this->post->orderBy('created_at','DESC')->where('user_id',$this->user()->id)->paginate(10);

    $itemsTransformed = $itemsPaginated
        ->getCollection()
        ->map(function($item) {
            return [
                'id' => $item->id,
                'contents' => $item->contents,
            ];
    })->toArray();

    return $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
        $itemsTransformed,
        $itemsPaginated->total(),
        $itemsPaginated->perPage(),
        $itemsPaginated->currentPage(), [
            'path' => \Request::url(),
            'query' => [
                'page' => $itemsPaginated->currentPage()
            ]
        ]
    );
  }

  /**
   * Description: Others Post Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_other($request) {
    $itemsPaginated = $this->post->orderBy('created_at','DESC')->where('user_id','!=',$this->user()->id)->paginate(10);

    $itemsTransformed = $itemsPaginated
        ->getCollection()
        ->map(function($item) {
            return [
                'id' => $item->id,
                'contents' => $item->contents,
            ];
    })->toArray();

    return $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
        $itemsTransformed,
        $itemsPaginated->total(),
        $itemsPaginated->perPage(),
        $itemsPaginated->currentPage(), [
            'path' => \Request::url(),
            'query' => [
                'page' => $itemsPaginated->currentPage()
            ]
        ]
    );
  }

  /**
   * Description: Others Post Paginated
   * Date: November 17, 2019 (Sunday)
   **/
  public function paginated_other_pics($id) {
    $itemsPaginated = $this->post->orderBy('created_at','DESC')->where('user_id',$id)->paginate(10);

    $itemsTransformed = $itemsPaginated
        ->getCollection()
        ->map(function($item) {
            return [
                'id' => $item->id,
                'contents' => $item->contents,
            ];
    })->toArray();

    return $itemsTransformedAndPaginated = new \Illuminate\Pagination\LengthAwarePaginator(
        $itemsTransformed,
        $itemsPaginated->total(),
        $itemsPaginated->perPage(),
        $itemsPaginated->currentPage(), [
            'path' => \Request::url(),
            'query' => [
                'page' => $itemsPaginated->currentPage()
            ]
        ]
    );
  }

  /**
   * Description: User's Posts
   * Date: November 18, 2019 (Monday)
   **/
  public function user_posts($id) {
    return $this->post->where('user_id',$id)->orderBy('created_at','DESC')->get();
  }

  /**
   * Description: View Hashtag
   * Date: December 04, 2019 (Wednesday)
   **/
  public function view_hashtag($key) {
    return $results = $this->hashtag
      ->selectRaw('id,hashtag')
      ->where('hashtag','like',strtolower(str_replace('#', '', $key)))
      ->first()->makeHidden('posts_paginated');
  }

  /**
   * Description: View Hashtag Paginated
   * Date: December 04, 2019 (Wednesday)
   **/
  public function paginated_view_hashtag($key) {
    return $results = $this->hashtag
      ->selectRaw('id,hashtag')
      ->where('hashtag','like',strtolower(str_replace('#', '', $key)))
      ->first()->makeHidden('posts');
  }

  /**
   * Description: Other User Feed
   * Date: December 05, 2019 (Thursday)
   **/
  public function other_user_feed($id) {
    $hts = HashtagFollowing::where('user_id',$id)->get();
    $ffs = Follower::where('user_id',$id)->get();

    $post = $this->post->orderBy('created_at','DESC')
    ->orWhere('user_id',$id);

    foreach ($hts as $ht) {
      $post = $post->orWhere('caption','like','%#'.$ht->hashtag->hashtag.'%');
    }

    foreach ($ffs as $ff) {
      $post = $post->orWhere('user_id',$ff->following->id);
    }
    $post = $post->get();
    return $post;
  }

  /**
   * Description: Save every hashtags on caption
   * Date: December 05, 2019 (Thursday)23:33:03
   **/
  public function save_hashtags($caption) {
    preg_match_all('/#(\w+)/', $caption, $matches);
    $i=0;
    foreach ($matches[1] as $hashtag) {
      if (Hashtag::where('hashtag',strtolower($hashtag))->count()==0) {
        $hashtags[$i++] = Hashtag::create(['hashtag'=>strtolower($hashtag)]);
      }
    }
  }


  /**
   * Description: Search Hashtag
   * Date: December 06, 2019 (Friday)
   **/
  public function search_hashtag($key) {
    return $results = $this->hashtag
      ->selectRaw('id,hashtag')
      ->where('hashtag','like',strtolower(str_replace('#', '', $key)).'%')
      ->get()
      ->take(5)
      ->makeHidden('posts');
  }
}
