<?php

namespace Domain\Like;

use Application\Repository;
use Domain\Comments\Comment;
use Domain\Like\Like;
use Domain\Notification\NotificationRepository;
use Domain\Posts\Post;

class LikeRepository extends Repository
{
  protected $like, $notify;

  public function __construct(Like $like, NotificationRepository $notify)
  {
      parent::__construct($this->like = $like);
      $this->notify = $notify;
  }

  /**
   * Description: Like Post
   * Date: September 21, 2019 (Saturday)
   **/
  public function like($id,$type)
  {
    if ($type=='post') {
      $likeable_type='Domain\Posts\Post';
      $item = Post::find($id);
    } else if ($type=='comment') {
      $likeable_type='Domain\Comments\Comment';
      $item = Comment::find($id);
    }
    $check = $this->like->where('likeable_id',$id)
      ->where('likeable_type',$likeable_type)
      ->where('user_id',auth()->user()->id)
      ->count();
      if ($check == 0) {
        $data = [
          'user_id'=>auth()->user()->id,
          'likeable_id'=>$id,
          'likeable_type'=>$likeable_type
        ];
        $source = $item->likes()->create([
          'user_id'=>auth()->user()->id
        ]);
        $this->notify->notify($type,'like',$id,$source->id);
        return response()->json(['message'=>'Like']);
      } else {
        foreach ($item->likes as $like) {
          $like->notifs_sent()->forceDelete();
        }
        $item->likes()->forceDelete();
        return response()->json(['message'=>'Unlike']);
      }
  }
}
