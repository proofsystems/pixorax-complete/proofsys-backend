<?php

namespace Domain\Like;

use App\User;
use Application\EloquentModel;
use Domain\Notification\Notification;

class Like extends EloquentModel
{
  protected $table = 'user_likes';

  protected $fillable =
  [
    'user_id',
    'likeable_id',
    'likeable_type',
  ];

  protected $with =['user'];

  public function likeable()
  {
    return $this->morphTo();
  }

  public function notifs_sent() {
    return $this->morphMany(Notification::class, 'sourceable');
  }

  public function user()
  {
    return $this->belongsTo(User::class,'user_id');
  }

  public static function boot()
  {
      parent::boot();

      static::deleting(function ($model) {
          $model->notifs_sent()->forceDelete();
      });
  }
}
