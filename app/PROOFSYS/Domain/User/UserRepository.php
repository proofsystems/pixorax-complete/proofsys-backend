<?php

namespace Domain\User;

use App\User as User;
use Application\Repository;
use Domain\Follower\Follower;
use Illuminate\Support\Facades\Hash;

class UserRepository extends Repository{

  public function __construct(User $user,Follower $follower){
    parent::__construct($user);
    $this->user = $user;
    $this->follower = $follower;
  }

  /**
   * Description: Get Token
   * Date: September 03, 2019 (Tuesday)
   * Developed by Jerwin Arnado <jerwin@hybrain.co>
   **/
  public function token($request) {
    $http = new \GuzzleHttp\Client;
    try {
      $response = $http->post(config('services.passport.login_endpoint'), [
        'form_params' => [
          'grant_type' => 'password',
          'client_id' => config('services.passport.client_id'),
          'client_secret' => config('services.passport.client_secret'),
          'username' => $request->email,
          'password' => $request->password,
        ]
      ]);
      return json_decode((string) $response->getBody(), true);
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      return $e->getCode();
    }
  }

  /**
   * Description: Get Profile
   * Date: September 03, 2019 (Tuesday)
   * Developed by Jerwin Arnado <jerwin@hybrain.co>
   **/
  public function profile($token) {
    $http = new \GuzzleHttp\Client;
    $response = $http->request('GET', config('services.passport.login_user'), [
      'headers' => [
        'Accept' => 'application/json',
        'Authorization' => 'Bearer '.$token,
      ],
    ]);
      return json_decode((string) $response->getBody(), true);
  }

  /**
   * Description: Register User
   * Date: August 09, 2019 (Friday)
   * Developed by Jerwin Arnado <jerwin@hybrain.co>
   **/
  public function register($request) {
    return User::create([
      'fname' => $request->fname,
      'lname' => $request->lname,
      'email' => $request->email,
      'username' => $request->username,
      'country_id' => $request->country_id,
      'password' => Hash::make($request->password),
    ]);
  }

  /**
   * Description: Find Username
   * Date: November 13, 2019 (Wednesday)
   **/
  public function findForPassport($input) {
    return $this->user
      ->where('username', $input)
      ->orWhere('email', $input)
    ->first();
  }

  /**
   * Description: Login Social
   * Date: September 03, 2019 (Tuesday)
   * Developed by Jerwin Arnado <jerwin@hybrain.co>
   **/
  public function social($request) {
    $http = new \GuzzleHttp\Client;
    try {
      $response = $http->post(config('services.passport.login_endpoint'), [
        'form_params' => [
          'grant_type' => 'social',
          'client_id' => config('services.passport.client_id'),
          'client_secret' => config('services.passport.client_secret'),
          'provider' => $request->provider,
          'access_token' => $request->access_token,
        ]
      ]);
      return json_decode((string) $response->getBody(), true);
    } catch (\GuzzleHttp\Exception\BadResponseException $e) {
      if ($e->getCode() === 400) {
        return response()->json('Invalid Request.', $e->getCode());
      } else if ($e->getCode() === 401) {
        return response()->json('Please try again', $e->getCode());
      }
      return response()->json('Something went wrong on the server.', $e->getCode());
    }
  }

  /**
   * Description: Update User
   * Date: September 27, 2019 (Friday)
   **/
  public function updateUser(array $data)
  {
    $entity = User::find(auth('api')->user()->id);

    foreach($data as $key => $input){
      $entity->{$key} = $input;
    }
    return $entity->save();
  }

  /**
   * Description: Suggestions
   * Date: November 12, 2019 (Tuesday)
   **/
  public function suggestions() {
    $following = $this->follower->selectRaw('following_user_id')->where('user_id',auth('api')->user()->id)->get();

    $suggestions = $this->user->where('id','!=',auth('api')->user()->id);

    if ($following->count()>0) {
      $i=0;
      foreach ($following as $user) {
        $users[$i++]=$user->following_user_id;
      }
    $suggestions = $suggestions->whereNotIn('id',$users);
    }

    $suggestions = $suggestions->inRandomOrder()->get();
    return $suggestions;
  }

  /**
   * Description: User Search
   * Date: November 12, 2019 (Tuesday)
   **/
  public function search($request) {
    if ($request->keyword) {
      return $this->user
        ->where('id','!=',auth('api')->user()->id)
        ->where(function($q) use ($request) {
          $q->where('fname','like',$request->keyword.'%')
          ->orWhere('lname','like',$request->keyword.'%')
          ->orWhere('email','like',$request->keyword);
        })
        ->get();
    }
  }

}
