<?php

namespace Domain\Comments;

use Application\Repository;
use Domain\Comments\Comment;

class CommentRepository extends Repository
{

  protected $comment;

  public function __construct(Comment $comment)
  {
      parent::__construct($this->comment = $comment);
  }
}
