<?php

namespace Domain\Comments;

use App\User;
use Application\EloquentModel;
use Domain\Like\Like;
use Domain\Notification\Notification;
use Domain\Posts\Post;

class Comment extends EloquentModel
{
    protected $table = 'comments';

    protected $fillable =
    [
        'user_id',
        'post_id',
        'comment'
    ];

    protected $with = ['user','likes'];
    protected $withCount = ['likes'];

    protected $appends = ['is_liked'];

    protected $cascadeDeleteMorph = ['likes', 'notifications'];

    public function likes()
    {
        return $this->morphMany(Like::class, 'likeable');
    }

    public function notifications()
    {
        return $this->morphMany(Notification::class, 'notifiable');
    }

    public function notifs_sent() {
        return $this->morphMany(Notification::class, 'sourceable');
    }

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Description: return if liked
     * Date: November 17, 2019 (Sunday)
     **/
    public function getIsLikedAttribute() {
        return $this->likes()->where(['likeable_id'=>$this->id,'user_id'=>$this->user_id])->count();
    }

    public static function boot()
    {
        parent::boot();

        static::deleting(function ($model) {
            $model->notifications()->forceDelete();
            $model->notifs_sent()->forceDelete();
            $model->likes()->forceDelete();
        });
    }
}


