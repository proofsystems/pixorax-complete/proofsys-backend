<?php

namespace Domain\Country;

use Application\EloquentModel;

class Country extends EloquentModel
{
    protected $table = 'countries';

    protected $fillable =
    [
        'country_name',
    ];
}


