<?php

namespace Domain\Country;

use Application\Repository;
use Domain\Country\Country;

class CountryRepository extends Repository
{

  protected $comment;

  public function __construct(Country $country)
  {
      parent::__construct($this->country = $country);
  }
}
