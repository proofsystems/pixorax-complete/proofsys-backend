<?php

namespace Domain\Story;

use Application\EloquentModel;
use Application\Traits\Imaging;
use Domain\Notification\Notification;
use Domain\Story\Story;

class Story extends EloquentModel
{
    use Imaging;

    protected $table = 'stories';

    protected $fillable =
    [
        'user_id',
        'content',
        'type',
        'recent'
    ];

    protected $image_fields = ['content'];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            static::storeImage($model);
            $model->user_id = Auth()->user()->id;
        });

        static::updating(function ($model) {
            static::updateImage($model);
        });

        static::deleting(function ($model) {
            static::deleteImage($model);
        });
    }
}



