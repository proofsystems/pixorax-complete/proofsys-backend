<?php

namespace Domain\Story;

use App\User;
use Application\Repository;
use Carbon\Carbon;
use Domain\Story\Story;
use Domain\ViewedStory\ViewedStory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoryRepository extends Repository
{

  protected $story;

  public function __construct(Story $story, ViewedStory $vs){
    parent::__construct($story);
    $this->vs = $vs;
    $this->story = $story;
  }

  /**
   * Description: User's Stories
   * Date: October 07, 2019 (Monday)
   **/
  public function own() {
    return $this->story
    ->where('user_id',auth('api')->user()->id)
    ->whereRaw('created_at >= DATE_SUB(NOW(),INTERVAL 24 HOUR)')
    ->orderBy('created_at','ASC')
    ->get();
  }

  public function perUser() {
    return User::with(['story'=>function($q){
        $q->whereRaw('created_at >= DATE_SUB(NOW(),INTERVAL 24 HOUR)')
        ->orderBy('created_at','ASC');
    }])
      ->whereHas('story',function($q){
        $q->whereRaw('created_at >= DATE_SUB(NOW(),INTERVAL 24 HOUR)');
      })
      ->get();
  }

  /**
   * Description: Show Story
   * Date: November 13, 2019 (Wednesday)
   **/
  public function view_story($id) {
    $check = $this->vs->where(['story_id'=>$id,'user_id'=>Auth('api')->user()->id])->count();
    if ($check==0) {
      return response()->json($this->vs->create(['story_id'=>$id]),200);
    }
  }

  /**
   * Description: Get users who viewed the story.
   * Date: November 13, 2019 (Wednesday)
   **/
  public function get_view_story($id) {
    return $this->vs->where('story_id',$id)->get();
  }
}
