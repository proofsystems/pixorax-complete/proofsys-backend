<?php

namespace App;

use App\Notifications\PasswordResetNotification;
use Application\Traits\Imaging;
use Domain\Country\Country;
use Domain\Follower\Follower;
use Domain\LinkedSocialAccount\LinkedSocialAccount;
use Domain\Posts\Post;
use Domain\Story\Story;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,SoftDeletes, Notifiable;
    use Imaging;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname','lname', 'email', 'username', 'password','image','country_id','cover_photo','description'
    ];

    protected $image_fields = ['image','cover_photo'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = [
        'image_address',
        'social_account',
        'is_followed',
        'num_post',
        'has_stories',
    ];

    protected $with =['country:id,country_name'];
    protected $withCount =['followers','following'];

    public static function boot()
    {
      parent::boot();

      static::creating(function($model){
        static::storeImage($model);
      });

      static::updating(function($model){
        static::updateImage($model);
      });

      static::deleting(function ($model) {
        static::deleteImage($model);
      });
    }

  public function story() {
    return $this->hasMany(Story::class,'user_id');
  }


  /**
   * Description: Followers
   * Date: October 30, 2019 (Wednesday)
   **/
  public function followers() {
    return $this->hasMany(Follower::class,'following_user_id','id');
  }

  /**
   * Description: Followers
   * Date: October 30, 2019 (Wednesday)
   **/
  public function following() {
    return $this->hasMany(Follower::class,'user_id','id');
  }

  public function getImageAddressAttribute(){
    return asset('/storage/' . $this->image);
  }

  public function getFullNameAttribute(){
    return $this->fname.' '.$this->lname;
  }

  public function country() {
    return $this->belongsTo(Country::class,'country_id');
  }

  /**
   * Description: isFollowed
   * Date: November 13, 2019 (Wednesday)
   **/
  public function getIsFollowedAttribute() {
    return Follower::where('user_id',auth('api')->user()->id)
      ->where('following_user_id',$this->id)->count();
  }

  /**
   * Send the password reset notification.
   *
   * @param  string  $token
   * @return void
   */
  public function sendPasswordResetNotification($token)
  {
    $this->notify(new PasswordResetNotification($token));
  }

  public function linkedSocialAccounts()
  {
    return $this->hasOne(LinkedSocialAccount::class);
  }

  public function getSocialAccountAttribute() {
    return $this->linkedSocialAccounts;
  }

  /**
   * Description: Number of Post
   * Date: November 17, 2019 (Sunday)
   **/
  public function getNumPostAttribute() {
    return Post::where('user_id',$this->id)->count();
  }

  /**
   * Description: Return Stories
   * Date: November 21, 2019 (Thursday)
   **/
  public function getHasStoriesAttribute() {
    return $this->story()->where('user_id',$this->id)
      ->whereRaw('created_at >= DATE_SUB(NOW(),INTERVAL 24 HOUR)')
      ->orderBy('created_at','ASC')
      ->get();
  }

}
