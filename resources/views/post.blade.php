<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <body class="container">
        <div class="card">
            <div class="card-body">
                <form action="{{route('webStore')}}" class="form" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="">Caption</label>
                        <input type="text" name="caption" class="form-control" required="">
                    </div>
                    <div class="form-group">
                        <label for="">Image</label>
                        <input type="file" name="image" id="" class="form-control">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Upload" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
