<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateHashtagFollowing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hashtag_following', function (Blueprint $table) {
            $table->dropColumn('hashtag');
            $table->bigInteger('hashtag_id')->unsigned()->nullable()->after('user_id');
            $table->foreign('hashtag_id')->references('id')->on('hashtags');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hashtag_following', function (Blueprint $table) {
            $table->string('hashtag')->nullable()->after('user_id');
            $table->dropForeign('hashtag_following_hashtag_id_foreign');
            $table->dropIndex('hashtag_following_hashtag_id_foreign');
            $table->dropColumn('hashtag_id');
        });
    }
}
