<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        $users = [
            [
              'fname'     =>    'Admin',
              'lname'     =>    'Admin',
              'email'     =>    'admin@prox.com',
              'password'  =>    bcrypt('123456')
            ]
        ];


        foreach($users as $item){
          $user = User::where('email',$item['email'])->first();

          if(!$user){
            User::create($item);
          }
        }
    }
}
